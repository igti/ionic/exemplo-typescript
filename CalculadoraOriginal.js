/**
 * Este arquivo não está sendo monitorado
 * pelo TypeScript e representa a forma
 * original da classe Calculadora
 */
class CalculadoraOriginal {

  /**
   * Getter para operando1
   */
  get operando1() {
    return this._operando1;
  }

  /**
   * Getter para operando2
   */
  get operando2() {
    return this._operando2;
  }

  /**
   * Construtor
   * @param {*} op1
   * @param {*} op2
   */
  constructor(op1, op2) {

    /**
     * Em ES5, os atributos da classe
     * são definidos no construtor
     */
    this._operando1 = op1;
    this._operando2 = op2;
  }

  /**
   * Função de soma
   */
  somar() {
    return this._operando1 + this._operando2;
  }

  /**
   * Função de subtração
   */
  subtrair() {
    return this._operando1 - this._operando2;
  }

  /**
   * Função estática de adição de valor
   * em vetores
   * @param {*} valor
   * @param {*} listaNumeros
   */
  static aplicarSomaEmNumeros(valor, listaNumeros) {
    var resultado = listaNumeros;
    for(var i = 0; i <= listaNumeros.length; i++)
      resultado[i] += valor;
    return resultado;
  }
}

/**
 * Instâncias: apenas a terceira é válida
 */
//const calculadoraJS = new CalculadoraOriginal('a', 'b');
//const calculadoraJS = new Calculadora();
const calculadoraJS = new Calculadora(8, 5);

//Quebrando linha para fins estéticos
console.log('');

console.log(
  `JavaScript: soma entre ${calculadoraJS.operando1} ` +
  `e ${calculadoraJS.operando2}: ` +
  `${calculadoraJS.somar()}`
);

console.log(
  `JavaScript: subtração entre ${calculadoraJS.operando1} ` +
  `e ${calculadoraJS.operando2}: ` +
  `${calculadoraJS.subtrair()}`
);

console.log(
  `JavaScript: adicionando 10 aos itens: [2, 4, 6, 8]: `  +
  `${Calculadora.aplicarSomaEmNumeros(10, [2, 4, 6, 8])}`
);
