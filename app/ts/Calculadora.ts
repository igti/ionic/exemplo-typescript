class Calculadora {

  /**
   * Em TypeScript, podemos
   * definir a visibilidade
   * de atributos
   */
  private _operando1: number;
  private _operando2: number;

  /**
   * Getter para operando1
   */
  get operando1() {
    return this._operando1;
  }

  /**
   * Getter para operando2
   */
  get operando2() {
    return this._operando2;
  }

  /**
   * Construtor
   * @param op1
   * @param op2
   */
  constructor(op1: number, op2: number) {

    this._operando1 = op1;
    this._operando2 = op2;
  }

  /**
   * Função de soma com arrow function
   */
  somar = () => {
    return this._operando1 + this._operando2;
  }

  /**
   * Função de soma com arrow function
   */
  subtrair = () => {
    return this._operando1 - this._operando2;
  }

  /**
   * Função estática de aplicação de soma
   * com arrow function abreviada
   * @param valor
   * @param listaNumeros
   */
  static aplicarSomaEmNumeros(valor: number, listaNumeros: number[]) {
    return listaNumeros.map(numero => numero + valor);
  }
}

/**
 * Instâncias: apenas a terceira é válida
 */
//const semParametro = new Calculadora();
//const parametroInvalido = new Calculadora(8, true);
const calculadoraTS = new Calculadora(3, 5);

console.log(
  `TypeScript: soma entre ${calculadoraTS.operando1} ` +
  `e ${calculadoraTS.operando2}: ` +
  `${calculadoraTS.somar()}`
);

console.log(
  `TypeScript: subtração entre ${calculadoraTS.operando1} ` +
  `e ${calculadoraTS.operando2}: ` +
  `${calculadoraTS.subtrair()}`
);

console.log(
  `TypeScript: adicionando 10 aos itens: [2, 4, 6, 8]: `  +
  `${Calculadora.aplicarSomaEmNumeros(10, [2, 4, 6, 8])}`
);
