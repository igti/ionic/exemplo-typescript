var Calculadora = /** @class */ (function () {
    function Calculadora(op1, op2) {
        var _this = this;
        this.somar = function () {
            return _this._operando1 + _this._operando2;
        };
        this.subtrair = function () {
            return _this._operando1 - _this._operando2;
        };
        this._operando1 = op1;
        this._operando2 = op2;
    }
    Object.defineProperty(Calculadora.prototype, "operando1", {
        get: function () {
            return this._operando1;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Calculadora.prototype, "operando2", {
        get: function () {
            return this._operando2;
        },
        enumerable: true,
        configurable: true
    });
    Calculadora.aplicarSomaEmNumeros = function (valor, listaNumeros) {
        return listaNumeros.map(function (numero) { return numero + 10; });
    };
    return Calculadora;
}());
var calculadoraTS = new Calculadora(3, 5);
//const semParametro = new Calculadora();
//const parametroInvalido = new Calculadora(8, true);
console.log("TypeScript: soma entre " + calculadoraTS.operando1 + " " +
    ("e " + calculadoraTS.operando2 + ": ") +
    ("" + calculadoraTS.somar()));
console.log("TypeScript: subtra\u00E7\u00E3o entre " + calculadoraTS.operando1 + " " +
    ("e " + calculadoraTS.operando2 + ": ") +
    ("" + calculadoraTS.subtrair()));
console.log("TypeScript: adicionando 10 aos itens: [2, 4, 6, 8]: " +
    ("" + Calculadora.aplicarSomaEmNumeros(10, [2, 4, 6, 8])));
